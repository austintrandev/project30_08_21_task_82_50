package com.shoppizza365.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.shoppizza365.model.*;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
	@Query(value = "SELECT * FROM customers  WHERE first_name like %:name% OR last_name like %:name%", nativeQuery = true)
	List<CCustomer> findCustomerByFirstOrLastName(@Param("name") String name);
	
	@Query(value = "SELECT * FROM customers  WHERE city like %:name% OR state like %:name%", nativeQuery = true)
	List<CCustomer> findCustomerByCityOrState(@Param("name") String name,Pageable pageable);
	
	@Query(value = "SELECT * FROM customers  WHERE country like %:name% ORDER BY first_name DESC", nativeQuery = true)
	List<CCustomer> findCustomerByCountryDESC(@Param("name") String name,Pageable pageable);
	
	@Query(value = "SELECT COUNT(c.id) FROM customers c WHERE c.country = 'USA'", nativeQuery = true)
	int getTotalCustomerFromUSA();
	
	@Query(value = "SELECT COUNT(c.id) FROM customers c WHERE c.country = 'France'", nativeQuery = true)
	int getTotalCustomerFromFrance();
	
	@Query(value = "SELECT COUNT(c.id) FROM customers c WHERE c.country = 'Singapore'", nativeQuery = true)
	int getTotalCustomerFromSingapore();
	
	@Query(value = "SELECT COUNT(c.id) FROM customers c WHERE c.country = 'Spain'", nativeQuery = true)
	int getTotalCustomerFromSpain();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE customers SET country = :country WHERE country IS NULL", nativeQuery = true)
	int updateCountry(@Param("country") String country);
}
